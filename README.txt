Drawfile
========

The drawfile module provides classes for reading and writing Acorn Drawfiles
and the objects contained within. It provides a solid foundation on which to
build conversion tools and is used by the drawinput filter, included with the
Skencil vector drawing application:

    http://www.skencil.org/

Please note that newer versions of the spritefile module and the spriteops
Pyrex extension module may be available at

    http://www.boddie.org.uk/david/Projects/Python/Spritefile/
