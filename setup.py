#! /usr/bin/env python

from distutils.core import setup, Extension
import os
from drawfile import version

try:

    from Pyrex.Distutils import build_ext
    use_pyrex = 1

except ImportError:

    use_pyrex = 0

if use_pyrex == 1:

    setup(
        name="drawfile",
        description="A module for reading and writing Acorn Drawfiles.",
        author="David Boddie",
        author_email="david@boddie.org.uk",
        url="http://www.boddie.org.uk/david/Projects/Python/Drawfile/",
        version=version,
        py_modules=["drawfile", "spritefile"],
        ext_modules=[Extension("spriteops", ["spriteops.pyx"])],
        cmdclass = {'build_ext': build_ext},
        scripts=["Tools/draw2xml.py"]
        )

else:

    setup(
        name="drawfile",
        description="A module for reading and writing Acorn Drawfiles.",
        author="David Boddie",
        author_email="david@boddie.org.uk",
        url="http://www.boddie.org.uk/david/Projects/Python/Drawfile/",
        version=version,
        py_modules=["drawfile", "spritefile"],
        scripts=["Tools/draw2xml.py"]
        )
