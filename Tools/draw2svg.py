#!/usr/bin/env python

# draw2svg.py
# A tool for converting Acorn Drawfiles to SVG files.
#
# Copyright (C) 2009 David Boddie <david@boddie.org.uk>
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA  02110-1301, USA.

import locale
from xml.dom import minidom
import drawfile

points_per_unit = 1.0/drawfile.units_per_point

Language_Country, Encoding = locale.getdefaultlocale()

class SVGWriter:

    def __init__(self):
    
        self.methods = \
        {
            drawfile.font_table: self.write_font_table,
            drawfile.text:       self.write_text,
            drawfile.path:       self.write_path,
            drawfile.sprite:     self.write_sprite,
            drawfile.group:      self.write_group,
            drawfile.tagged:     self.write_tagged,
            drawfile.text_area:  self.write_text_area,
            drawfile.column:     self.write_column,
            drawfile.options:    self.write_options,
            drawfile.jpeg:       self.write_jpeg,
            drawfile.unknown:    self.write_unknown
        }
    
    def process_attribute(self, name):
    
        pieces = name.split(" ")
        return "-".join(pieces)
    
    def set_colour_attribute(self, element, xml_attribute, value):
    
        element.setAttribute(
            xml_attribute, "#%02x%02x%02x" % tuple(value[1:])
            )
    
    def set_bounding_box(self, element, obj):
    
        element.setAttribute("x", u"%i" % ((obj.x1 - self.left) * points_per_unit))
        element.setAttribute("y", u"%i" % ((self.top - obj.y1) * points_per_unit))
        self.set_size(element, obj)
    
    def set_size(self, element, obj):
    
        element.setAttribute("width", u"%i" % ((obj.x2 - obj.x1) * points_per_unit))
        element.setAttribute("height", u"%i" % ((obj.y2 - obj.y1) * points_per_unit))
    
    def write(self, draw_file, file):
    
        self.left = draw_file.x1
        self.top = draw_file.y2
        
        self.document = minidom.Document()
        root = self.document.createElement("svg")
        root.setAttribute("version", "1.1")
        root.setAttribute("xmlns", "http://www.w3.org/2000/svg")
        self.set_size(root, draw_file)
        self.document.appendChild(root)
        
        self.write_objects(root, draw_file.objects)
        
        file.write(self.document.toprettyxml(indent = u"", encoding = "utf8"))
    
    def write_objects(self, parent, objects):
    
        for obj in objects:
        
            method = self.methods[obj.__class__]
            method(parent, obj)
    
    def write_font_table(self, parent, font_table):
    
        font_table_element = self.document.createElement("defs")
        
        for number, name in font_table.font_table.items():
        
            font_element = self.document.createElement("font")
            font_element.setAttribute("id", unicode(number))
            font_face_element = self.document.createElement("font-face")
            text_element = self.document.createTextNode(unicode(name, Encoding))
            font_face_element.appendChild(text_element)
            font_element.appendChild(font_face_element)
        
        parent.appendChild(font_element)
    
    def write_text(self, parent, text):
    
        text_element = self.document.createElement("text")
        
        self.set_bounding_box(text_element, text)
        self.set_colour_attribute(text_element, "fill", text.background)
        self.set_colour_attribute(text_element, "color", text.foreground)
        text_element.setAttribute("font", u"url(#"+unicode(text.style)+u")")
        #text_element.setAttribute("width", unicode(text.size[0]))
        #text_element.setAttribute("height", unicode(text.size[1]))
        #text_element.setAttribute("baselinex", unicode(text.baseline[0]))
        #text_element.setAttribute("baseliney", unicode(text.baseline[1]))
        
        text_content = self.document.createCDATASection(unicode(text.text, Encoding))
        text_element.appendChild(text_content)
        
        parent.appendChild(text_element)
    
    def write_path(self, parent, path):
    
        path_element = self.document.createElement("path")
        
        self.set_colour_attribute(path_element, "fill", path.fill)
        self.set_colour_attribute(path_element, "stroke", path.outline)
        path_element.setAttribute("stroke-width", u"%spt" % unicode(path.width * points_per_unit))
        
        #for name, value in path.style.items():
        #
        #    path_element.setAttribute(
        #        u"draw:"+self.process_attribute(name), unicode(value)
        #        )
        
        d = []
        for stroke in path.path:
        
            #if stroke[0] == "end":
            #    d.append(u"z")
            if stroke[0] == "move":
                d.append(u"M %i %i" % ((stroke[1][0] - self.left) * points_per_unit, (self.top - stroke[1][1]) * points_per_unit))
            elif stroke[0] == "draw":
                d.append(u"L %i %i" % ((stroke[1][0] - self.left) * points_per_unit, (self.top - stroke[1][1]) * points_per_unit))
            elif stroke[0] == "bezier":
                d.append(u"C %i %i %i %i %i %i" % (
                    (stroke[1][0] - self.left) * points_per_unit, (self.top - stroke[1][1]) * points_per_unit,
                    (stroke[2][0] - self.left) * points_per_unit, (self.top - stroke[2][1]) * points_per_unit,
                    (stroke[3][0] - self.left) * points_per_unit, (self.top - stroke[3][1]) * points_per_unit))
            elif stroke[0] == "close":
                d.append(u"z")
            
        path_element.setAttribute(u"d", u" ".join(d))
        parent.appendChild(path_element)
    
    def write_sprite(self, parent, sprite):
    
        return
        sprite_element = self.document.createElement("draw:sprite")
        
        self.set_bounding_box(sprite_element, sprite)
        
        if hasattr(sprite, "transform"):
        
            transform_element = self.document.createElement("transform")
            transform_element.setAttribute("t11", unicode(sprite.transform[0]))
            transform_element.setAttribute("t12", unicode(sprite.transform[1]))
            transform_element.setAttribute("t21", unicode(sprite.transform[2]))
            transform_element.setAttribute("t22", unicode(sprite.transform[3]))
            transform_element.setAttribute("t31", unicode(sprite.transform[4]))
            transform_element.setAttribute("t32", unicode(sprite.transform[5]))
            sprite_element.appendChild(transform_element)
        
        sprite_element.setAttribute("name", unicode(sprite.name.strip(), Encoding))
        
        sprite_wrapper_element = self.document.createElement("image")
        sprite_wrapper_element.setAttribute("width", unicode(sprite.sprite["width"]))
        sprite_wrapper_element.setAttribute("height", unicode(sprite.sprite["height"]))
        sprite_wrapper_element.setAttribute("bpp", unicode(sprite.sprite["bpp"]))
        sprite_wrapper_element.setAttribute("mode", unicode(sprite.sprite["mode"]))
        sprite_wrapper_element.setAttribute("xdpi", unicode(sprite.sprite["dpi x"]))
        sprite_wrapper_element.setAttribute("ydpi", unicode(sprite.sprite["dpi y"]))
        
        sprite_data_element = self.document.createTextNode(
            " ".join(map(lambda c: unicode(ord(c)), sprite.sprite["image"]))
            )
        
        sprite_wrapper_element.appendChild(sprite_data_element)
        sprite_element.appendChild(sprite_wrapper_element)
        parent.appendChild(sprite_element)
    
    def write_group(self, parent, group):
    
        group_element = self.document.createElement("g")
        
        self.set_bounding_box(group_element, group)
        #group_element.setAttribute("draw:name", unicode(group.name.strip()))
        
        self.write_objects(group_element, group.objects)
        
        parent.appendChild(group_element)
    
    def write_tagged(self, parent, tagged):
    
        tagged_element = self.document.createElement("g")
        
        self.set_bounding_box(tagged_element, tagged)
        #tagged_element.setAttribute("draw:id", unicode(tagged.id))
        #tagged_element.setAttribute("draw:data", unicode(tagged.data))
        
        if tagged_object:
        
            self.write_objects(tagged_element, [tagged.object])
        
        parent.appendChild(tagged_element)
    
    def write_text_area(self, parent, text_area):
    
        return
        text_area_element = self.document.createElement("draw:textarea")
        
        self.set_bounding_box(text_area_element, text_area)
        self.set_colour_attribute(text_area_element, text_area.background)
        self.set_colour_attribute(text_area_element, text_area.foreground)
        
        for column in text_area.columns:
        
            self.write_column(text_area_element, column)
        
        text_element = self.document.createTextNode(unicode(text_area.text, Encoding))
        text_area_element.appendChild(text_element)
        parent.appendChild(text_area_element)
    
    def write_column(self, parent, column):
    
        column_element = self.document.createElement("column")
        self.set_bounding_box(column_element, column)
        parent.appendChild(column_element)
    
    def write_options(self, parent, options):
    
        return
        options_element = self.document.createElement("draw:options")
        
        for option, value in options.options.items():
        
            options_element.setAttribute(
                self.process_attribute(option), unicode(value, Encoding)
                )
        
        parent.appendChild(options_element)
    
    def write_jpeg(self, parent, jpeg):
    
        return
        jpeg_element = self.document.createElement("draw:jpeg")
        
        self.set_bounding_box(jpeg_element, jpeg)
        jpeg_element.setAttribute("xdpi", unicode(jpeg.dpi_x))
        jpeg_element.setAttribute("ydpi", unicode(jpeg.dpi_y))
        jpeg_element.setAttribute("length", unicode(jpeg.length))
        
        transform_element = self.document.createElement("transform")
        transform_element.setAttribute("t11", unicode(jpeg.transform[0]))
        transform_element.setAttribute("t12", unicode(jpeg.transform[1]))
        transform_element.setAttribute("t21", unicode(jpeg.transform[2]))
        transform_element.setAttribute("t22", unicode(jpeg.transform[3]))
        transform_element.setAttribute("t31", unicode(jpeg.transform[4]))
        transform_element.setAttribute("t32", unicode(jpeg.transform[5]))
        jpeg_element.appendChild(transform_element)
        
        image_element = self.document.createElement("image")
        data_element = self.document.createTextNode(unicode(jpeg.image))
        
        image_element.appendChild(data_element)
        jpeg_element.appendChild(image_element)
        parent.appendChild(jpeg_element)
    
    def write_unknown(self, parent, unknown):
    
        return
        unknown_element = self.document.createElement("draw:unknown")
        
        self.set_bounding_box(unknown_element, unknown)
        unknown_element.setAttribute("type", unicode(unknown.type))
        unknown_element.setAttribute("length", unicode(unknown.length))
        
        data_element = self.document.createTextNode(unknown.data)
        
        unknown_element.appendChild(data_element)
        parent.appendChild(unknown_element)

if __name__ == "__main__":

    import sys
    
    if not 3 <= len(sys.argv) <= 4:
    
        sys.stderr.write("Usage: %s [encoding] <Drawfile> <SVG file>\n" % sys.argv[0])
        sys.exit()
    
    if len(sys.argv) == 4:
        Encoding = sys.argv[1]
    
    try:
        draw_file = open(sys.argv[-2])
    
    except IOError:
        sys.stderr.write("Failed to open the Drawfile for reading: %s\n" % sys.argv[1])
        sys.exit(1)
    
    try:
        draw = drawfile.drawfile(draw_file)
    
    except drawfile.drawfile_error:
        draw_file.close()
        print "Failed to read the Drawfile: %s" % sys.argv[1]
        sys.exit(1)

    draw_file.close()
    
    try:
        xml_file = open(sys.argv[-1], "wb")
    
    except IOError:
        sys.stderr.write("Failed to open the SVG file for writing: %s\n" % sys.argv[2])
        sys.exit(1)
    
    try:
        writer = SVGWriter()
        writer.write(draw, xml_file)
    
    except IOError:
        xml_file.close()
        sys.stderr.write("Failed to write the SVG file: %s\n" % sys.argv[2])
        sys.exit(1)
    
    xml_file.close()
    
    sys.exit()
