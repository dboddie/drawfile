#!/usr/bin/env python

# draw2xml.py
# A tool for converting Acorn Drawfiles to an XML representation.
#
# (C) David Boddie 2001-2005
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA  02110-1301, USA.

from xml.dom import minidom
import drawfile

class XMLWriter:

    def __init__(self, encoding):
    
        self.encoding = encoding
        self.methods = \
        {
            drawfile.font_table: self.write_font_table,
            drawfile.text:       self.write_text,
            drawfile.path:       self.write_path,
            drawfile.sprite:     self.write_sprite,
            drawfile.group:      self.write_group,
            drawfile.tagged:     self.write_tagged,
            drawfile.text_area:  self.write_text_area,
            drawfile.column:     self.write_column,
            drawfile.options:    self.write_options,
            drawfile.jpeg:       self.write_jpeg,
            drawfile.unknown:    self.write_unknown
        }
    
    def process_attribute(self, name):
    
        pieces = name.split(" ")
        return "-".join(pieces)
    
    def set_colour_attribute(self, element, obj, attribute):
    
        element.setAttribute(
            attribute, "#%02x%02x%02x" % tuple(getattr(obj, attribute)[1:])
            )
    
    def set_bounding_box(self, element, obj):
    
        element.setAttribute("x1", unicode(obj.x1))
        element.setAttribute("y1", unicode(obj.y1))
        element.setAttribute("x2", unicode(obj.x2))
        element.setAttribute("y2", unicode(obj.y2))
    
    def write(self, draw_file, file):
    
        self.document = minidom.Document()
        root = self.document.createElement("drawfile")
        root.setAttribute("major", unicode(draw_file.version["major"]))
        root.setAttribute("minor", unicode(draw_file.version["minor"]))
        root.setAttribute("creator", unicode(draw_file.creator.strip(), self.encoding))
        self.set_bounding_box(root, draw_file)
        self.document.appendChild(root)
        
        self.write_objects(root, draw_file.objects)
        
        file.write(self.document.toprettyxml(indent = ""))
    
    def write_objects(self, parent, objects):
    
        for obj in objects:
        
            method = self.methods[obj.__class__]
            method(parent, obj)
    
    def write_font_table(self, parent, font_table):
    
        font_table_element = self.document.createElement("fonttable")
        
        for number, name in font_table.font_table.items():
        
            font_element = self.document.createElement("font")
            font_element.setAttribute("name", unicode(name.strip(), self.encoding))
            font_element.setAttribute("number", unicode(number))
            font_table_element.appendChild(font_element)
        
        parent.appendChild(font_table_element)
    
    def write_text(self, parent, text):
    
        text_element = self.document.createElement("text")
        
        self.set_bounding_box(text_element, text)
        self.set_colour_attribute(text_element, text, "background")
        self.set_colour_attribute(text_element, text, "foreground")
        text_element.setAttribute("style", unicode(text.style))
        text_element.setAttribute("width", unicode(text.size[0]))
        text_element.setAttribute("height", unicode(text.size[1]))
        text_element.setAttribute("baselinex", unicode(text.baseline[0]))
        text_element.setAttribute("baseliney", unicode(text.baseline[1]))
        
        text_content = self.document.createCDATASection(unicode(text.text, self.encoding))
        text_element.appendChild(text_content)
        
        parent.appendChild(text_element)
    
    def write_path(self, parent, path):
    
        path_element = self.document.createElement("path")
        
        self.set_bounding_box(path_element, path)
        self.set_colour_attribute(path_element, path, "fill")
        self.set_colour_attribute(path_element, path, "outline")
        path_element.setAttribute("width", unicode(path.width))
        
        for name, value in path.style.items():
        
            path_element.setAttribute(
                self.process_attribute(name), unicode(value)
                )
        
        for stroke in path.path:
        
            if stroke[0] == "end":
                stroke_element = self.document.createElement("end")
            elif stroke[0] == "move":
                stroke_element = self.document.createElement("move")
                stroke_element.setAttribute("x", unicode(stroke[1][0]))
                stroke_element.setAttribute("y", unicode(stroke[1][1]))
            elif stroke[0] == "draw":
                stroke_element = self.document.createElement("draw")
                stroke_element.setAttribute("x", unicode(stroke[1][0]))
                stroke_element.setAttribute("y", unicode(stroke[1][1]))
            elif stroke[0] == "bezier":
                stroke_element = self.document.createElement("bezier")
                stroke_element.setAttribute("cx1", unicode(stroke[1][0]))
                stroke_element.setAttribute("cy1", unicode(stroke[1][1]))
                stroke_element.setAttribute("cx2", unicode(stroke[2][0]))
                stroke_element.setAttribute("cy2", unicode(stroke[2][1]))
                stroke_element.setAttribute("x", unicode(stroke[3][0]))
                stroke_element.setAttribute("y", unicode(stroke[3][1]))
            elif stroke[0] == "close":
                stroke_element = self.document.createElement("close")
            
            path_element.appendChild(stroke_element)
        
        parent.appendChild(path_element)
    
    def write_sprite(self, parent, sprite):
    
        sprite_element = self.document.createElement("sprite")
        
        self.set_bounding_box(sprite_element, sprite)
        
        if hasattr(sprite, "transform"):
        
            transform_element = self.document.createElement("transform")
            transform_element.setAttribute("t11", unicode(sprite.transform[0]))
            transform_element.setAttribute("t12", unicode(sprite.transform[1]))
            transform_element.setAttribute("t21", unicode(sprite.transform[2]))
            transform_element.setAttribute("t22", unicode(sprite.transform[3]))
            transform_element.setAttribute("t31", unicode(sprite.transform[4]))
            transform_element.setAttribute("t32", unicode(sprite.transform[5]))
            sprite_element.appendChild(transform_element)
        
        sprite_element.setAttribute("name", unicode(sprite.name.strip(), self.encoding))
        
        sprite_wrapper_element = self.document.createElement("image")
        sprite_wrapper_element.setAttribute("width", unicode(sprite.sprite["width"]))
        sprite_wrapper_element.setAttribute("height", unicode(sprite.sprite["height"]))
        sprite_wrapper_element.setAttribute("bpp", unicode(sprite.sprite["bpp"]))
        sprite_wrapper_element.setAttribute("mode", unicode(sprite.sprite["mode"]))
        sprite_wrapper_element.setAttribute("xdpi", unicode(sprite.sprite["dpi x"]))
        sprite_wrapper_element.setAttribute("ydpi", unicode(sprite.sprite["dpi y"]))
        
        sprite_data_element = self.document.createTextNode(
            " ".join(map(lambda c: unicode(ord(c)), sprite.sprite["image"]))
            )
        
        sprite_wrapper_element.appendChild(sprite_data_element)
        sprite_element.appendChild(sprite_wrapper_element)
        parent.appendChild(sprite_element)
    
    def write_group(self, parent, group):
    
        group_element = self.document.createElement("group")
        
        self.set_bounding_box(group_element, group)
        group_element.setAttribute("name", unicode(group.name.strip(), self.encoding))
        
        self.write_objects(group_element, group.objects)
        
        parent.appendChild(group_element)
    
    def write_tagged(self, parent, tagged):
    
        tagged_element = self.document.createElement("tagged")
        
        self.set_bounding_box(tagged_element, tagged)
        tagged_element.setAttribute("id", unicode(tagged.id))
        tagged_element.setAttribute("data", unicode(tagged.data))
        
        if tagged_object:
        
            self.write_objects(tagged_element, [tagged.object])
        
        parent.appendChild(tagged_element)
    
    def write_text_area(self, parent, text_area):
    
        text_area_element = self.document.createElement("textarea")
        
        self.set_bounding_box(text_area_element, text_area)
        self.set_colour_attribute(text_area_element, text_area, "background")
        self.set_colour_attribute(text_area_element, text_area, "foreground")
        
        for column in text_area.columns:
        
            self.write_column(text_area_element, column)
        
        text_element = self.document.createTextNode(unicode(text_area.text, self.encoding))
        text_area_element.appendChild(text_element)
        parent.appendChild(text_area_element)
    
    def write_column(self, parent, column):
    
        column_element = self.document.createElement("column")
        self.set_bounding_box(column_element, column)
        parent.appendChild(column_element)
    
    def write_options(self, parent, options):
    
        options_element = self.document.createElement("options")
        
        for option, value in options.options.items():
        
            options_element.setAttribute(
                self.process_attribute(option), unicode(value)
                )
        
        parent.appendChild(options_element)
    
    def write_jpeg(self, parent, jpeg):
    
        jpeg_element = self.document.createElement("jpeg")
        
        self.set_bounding_box(jpeg_element, jpeg)
        jpeg_element.setAttribute("xdpi", unicode(jpeg.dpi_x))
        jpeg_element.setAttribute("ydpi", unicode(jpeg.dpi_y))
        jpeg_element.setAttribute("length", unicode(jpeg.length))
        
        transform_element = self.document.createElement("transform")
        transform_element.setAttribute("t11", unicode(jpeg.transform[0]))
        transform_element.setAttribute("t12", unicode(jpeg.transform[1]))
        transform_element.setAttribute("t21", unicode(jpeg.transform[2]))
        transform_element.setAttribute("t22", unicode(jpeg.transform[3]))
        transform_element.setAttribute("t31", unicode(jpeg.transform[4]))
        transform_element.setAttribute("t32", unicode(jpeg.transform[5]))
        jpeg_element.appendChild(transform_element)
        
        image_element = self.document.createElement("image")
        data_element = self.document.createTextNode(unicode(jpeg.image))
        
        image_element.appendChild(data_element)
        jpeg_element.appendChild(image_element)
        parent.appendChild(jpeg_element)
    
    def write_unknown(self, parent, unknown):
    
        unknown_element = self.document.createElement("unknown")
        
        self.set_bounding_box(unknown_element, unknown)
        unknown_element.setAttribute("type", unicode(unknown.type))
        unknown_element.setAttribute("length", unicode(unknown.length))
        
        data_element = self.document.createTextNode(unknown.data)
        
        unknown_element.appendChild(data_element)
        parent.appendChild(unknown_element)

if __name__ == "__main__":

    import sys
    
    if not 3 <= len(sys.argv) <= 4:
    
        print "Usage: %s [encoding] <Drawfile> <XML file>" % sys.argv[0]
        sys.exit()
    
    elif len(sys.argv) == 4:
    
        encoding = sys.argv[1]
        draw_file_path = sys.argv[2]
        xml_file_path = sys.argv[3]
    
    else:
        encoding = sys.getdefaultencoding()
        draw_file_path = sys.argv[1]
        xml_file_path = sys.argv[2]
    
    try:
    
        draw_file = open(draw_file_path)
    
    except IOError:
    
        print "Failed to open the Drawfile for reading: %s" % draw_file_path
        sys.exit(1)
    
    try:
    
        draw = drawfile.drawfile(draw_file)
    
    except drawfile.drawfile_error:
    
        draw_file.close()
        print "Failed to read the Drawfile: %s" % draw_file_path
        sys.exit(1)

    draw_file.close()
    
    try:
    
        xml_file = open(xml_file_path, "wb")
    
    except IOError:
    
        print "Failed to open the XML file for writing: %s" % xml_file_path
        sys.exit(1)
    
    try:
    
        writer = XMLWriter(encoding)
        writer.write(draw, xml_file)
    
    except IOError:
    
        xml_file.close()
        print "Failed to write the XML file: %s" % xml_file_path
        sys.exit(1)
    
    xml_file.close()
    
    sys.exit()
