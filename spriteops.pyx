
# spriteops.pyx
# Replacement compiled functions for the spritefile module.
#
# Copyright (C) 2002-2005 David Boddie
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA  02110-1301, USA.

cdef extern from "Python.h":

    (char *) PyString_AsString(object obj)
    int PyString_Size(object obj)
    object PyString_FromStringAndSize(char *s, int len)
    (void *) PyMem_Malloc(int)
    (void *) PyMem_Realloc(void *, int)
    (void) PyMem_Free(void *)

# scaling factor for 8 bits per pixel colour components
cdef float scale8
  
scale8 = 255.0/15.0

# scaling factor for 16 bits per pixel colour components
cdef float scale16
 
scale16 = 255.0/31.0

palette16 = [
            (0xff, 0xff, 0xff), (0xdd, 0xdd, 0xdd),
            (0xbb, 0xbb, 0xbb), (0x99, 0x99, 0x99),
            (0x77, 0x77, 0x77), (0x55, 0x55, 0x55),
            (0x33, 0x33, 0x33), (0x00, 0x00, 0x00),
            (0x00, 0x44, 0x99), (0xee, 0xee, 0x00),
            (0x00, 0xcc, 0x00), (0xdd, 0x00, 0x00),
            (0xee, 0xee, 0xbb), (0x55, 0x88, 0x00),
            (0xff, 0xbb, 0x00), (0x00, 0xbb, 0xff)
        ]

palette4 = [
            (0xff, 0xff, 0xff), (0xbb, 0xbb, 0xbb),
            (0x77, 0x77, 0x77), (0x00, 0x00, 0x00)
        ]
    
cdef object _sprite2rgb(char *data, int width, int height, int h_words, int first_bit_used,
                      int bpp, object palette):

    # Convert sprite to RGB values
    cdef int has_palette
    
    # Length of the output string
    cdef int length
    
    # Bit offset into the string, bit offset into the current row and byte pointer to
    # the current data
    cdef int ptr, row_ptr, data_ptr
    
    # Column and row counters
    cdef int i, j
    
    # Red, green and blue components and temporary variables
    cdef int red, green, blue, value
    
    # Output string
    cdef char *rgb

    # Pointer into the output string
    cdef int k
    
    # Return object
    cdef object obj
    
    if palette != []:
        has_palette = 1
    else:
        has_palette = 0

    # Calculate the length of the output string.
    length = width * height * 3
    
    # Allocate memory for the output string
    rgb = <char *>PyMem_Malloc(length)
    
    # bit offset
    ptr = 0
    
    # Output pointer
    k = 0
    
    for j from 0 <= j < height:

        # bit offset into the image
        row_ptr = ptr + first_bit_used

        for i from 0 <= i < width:

            data_ptr = row_ptr >> 3

            # Conversion depends on bpp value
            if bpp == 32:

                rgb[k]     = data[data_ptr]
                rgb[k + 1] = data[data_ptr + 1]
                rgb[k + 2] = data[data_ptr + 2]
                
                row_ptr = row_ptr + 32

            elif bpp == 16:

                value = (<int>data[data_ptr] & 0xff) | \
                        ((<int>data[data_ptr + 1] & 0xff) << 8)
                
                red   = <int>( (value & 0x1f) * scale16 )
                green = <int>( ((value >> 5) & 0x1f) * scale16 )
                blue  = <int>( ((value >> 10) & 0x1f) * scale16 )
                
                rgb[k]     = <char>red
                rgb[k + 1] = <char>green
                rgb[k + 2] = <char>blue
                
                row_ptr = row_ptr + 16

            elif bpp == 8:

                if has_palette == 0:

                    # Standard VIDC 256 colours
                    value = <int>data[data_ptr]
                    red   = ((value & 0x10) >> 1) | (value & 7)
                    green = ((value & 0x40) >> 3) | \
                            ((value & 0x20) >> 3) | (value & 3)
                    blue  = ((value & 0x80) >> 4) | \
                            ((value & 8) >> 1) | (value & 3)
                    
                    red   = <int>( red * scale8 )
                    green = <int>( green * scale8 )
                    blue  = <int>( blue * scale8 )
                
                    rgb[k]     = <char>red
                    rgb[k + 1] = <char>green
                    rgb[k + 2] = <char>blue
                
                else:
                    # 256 entry palette
                    value = <int>data[data_ptr]
                    
                    red = (palette[value][0][0])
                    green = (palette[value][0][1])
                    blue = (palette[value][0][2])

                    rgb[k]     = <char>red
                    rgb[k + 1] = <char>green
                    rgb[k + 2] = <char>blue
                
                row_ptr = row_ptr + 8

            elif bpp == 4:

                value = ( (<int>data[data_ptr]) >> (row_ptr % 8) ) & 0xf

                if has_palette == 0:

                    # Standard 16 desktop colours
                    # Look up the value in the standard palette
                    red = palette16[value][0]
                    green = palette16[value][1]
                    blue = palette16[value][2]
                else:
                    # 16 entry palette
                    red = palette[value][0][0]
                    green = palette[value][0][1]
                    blue = palette[value][0][2]

                rgb[k]     = <char>red
                rgb[k + 1] = <char>green
                rgb[k + 2] = <char>blue
                    
                row_ptr = row_ptr + 4

            elif bpp == 2:

                value = ((<int>data[data_ptr]) >> (row_ptr % 8) ) & 0x3

                if has_palette == 0:

                    # Greyscales
                    red = palette4[value][0]
                    green = palette4[value][1]
                    blue = palette4[value][2]
                else:
                    # 4 entry palette
                    red = palette[value][0][0]
                    green = palette[value][0][1]
                    blue = palette[value][0][2]

                rgb[k]     = <char>red
                rgb[k + 1] = <char>green
                rgb[k + 2] = <char>blue
                
                row_ptr = row_ptr + 2

            elif bpp == 1:

                value = ((<int>data[data_ptr]) >> (row_ptr % 8) ) & 1

                if has_palette == 0:

                    # Black and white
                    red = green = blue = (255*(1-value))
                else:
                    # 2 entry palette
                    red = palette[value][0][0]
                    green = palette[value][0][1]
                    blue = palette[value][0][2]

                rgb[k]     = <char>red
                rgb[k + 1] = <char>green
                rgb[k + 2] = <char>blue
                
                row_ptr = row_ptr + 1

            k = k + 3

        ptr = ptr + (h_words * 32)

    obj = PyString_FromStringAndSize(rgb, length)
    
    PyMem_Free(rgb)
    
    return obj

def sprite2rgb(file, width, height, h_words, first_bit_used, bpp, palette):

    # Convert the sprite to RGB values
    
    # Read the required number of bytes based on the number of words per line and the
    # number of lines.
    data = file.read( (h_words * 4) * height )

    image = _sprite2rgb(data, width, height, h_words, first_bit_used, bpp, palette)

    return image


cdef object _mask2rgba(char *data, int width, int height, int row_size,
                       int first_bit_used, int bpp, char *image):

    # Length of the output string
    cdef int length
    
    # Bit offset into the string, bit offset into the current row and byte pointer to
    # the current mask data
    cdef int ptr, row_ptr, data_ptr
    
    # Byte pointer into the image data.
    cdef int image_ptr
    
    # Column and row counters
    cdef int i, j
    
    # Mask value
    cdef int value
    
    # Output string
    cdef char *rgba

    # Pointer into the output string
    cdef int k
    
    # Return object
    cdef object obj
    
    # Calculate the length of the output string.
    length = width * height * 4
    
    # Allocate memory for the output string
    rgba = <char *>PyMem_Malloc(length)
    
    # Bit offset into the mask data
    ptr = 0
    
    # Byte offset into the image data
    image_ptr = 0
    
    # Byte offset into the output string
    k = 0

    for j from 0 <= j < height:

        # Bit offset into the image
        row_ptr = ptr + first_bit_used

        for i from 0 <= i < width:

            data_ptr = row_ptr >> 3

            # Store image RGB values.
            rgba[k]     = image[image_ptr]
            rgba[k + 1] = image[image_ptr + 1]
            rgba[k + 2] = image[image_ptr + 2]
            
            # Conversion depends on bpp value
            if bpp == 32:

                value = ((<int>data[data_ptr]) >> (row_ptr % 8)) & 1
                value = value * 0xff
                rgba[k + 3] = <char>value
                
                row_ptr = row_ptr + 1

            elif bpp == 16:

                value = ((<int>data[data_ptr]) >> (row_ptr % 8)) & 1
                value = value * 0xff
                rgba[k + 3] = <char>value
                
                row_ptr = row_ptr + 1

            elif bpp == 8:

                rgba[k + 3] = data[data_ptr]
                
                row_ptr = row_ptr + 8

            elif bpp == 4:

                value = ( (<int>data[data_ptr]) >> (row_ptr % 8) ) & 0xf
                value = value | (value << 4)
                rgba[k + 3] = <char>value
                
                row_ptr = row_ptr + 4

            elif bpp == 2:

                value = ( (<int>data[data_ptr]) >> (row_ptr % 8) ) & 0x3
                if value == 3:
                    value = 0xff
                rgba[k + 3] = <char>value
                
                row_ptr = row_ptr + 2

            elif bpp == 1:

                # Black and white
                value = ((<int>data[data_ptr]) >> (row_ptr % 8) ) & 1
                value = value * 0xff
                rgba[k + 3] = <char>value
                
                row_ptr = row_ptr + 1
            
            image_ptr = image_ptr + 3
            k = k + 4

        ptr = ptr + (row_size * 32)

    obj = PyString_FromStringAndSize(rgba, length)
    
    PyMem_Free(rgba)
    
    return obj


def mask2rgba(file, width, height, first_bit_used, bpp, image):

    # Read mask data and add it to existing RGB data to create an RGBA image.
    
    # Determine the number of bytes used to store the mask data. This is determined
    # by the number of bits per pixel used to store the sprite.
    
    if bpp == 32 or bpp == 16:

        bpp = 1

    # Colour depths below 16 bpp have the same number of bpp in the mask
    bits = bpp * width

    # Calculate the number of words in each row of the mask.
    row_size = bits >> 5
    if bits % 32 != 0:
        row_size = row_size + 1

    # Read the mask data.
    data = file.read(row_size * 4 * height)
    
    # Create the RGBA image.
    image = _mask2rgba(
        data, width, height, row_size, first_bit_used, bpp, image
        )
    
    return image



cdef object _rgb2sprite(char *image, int width, int height, int h_words,
                        int bpp, object palette):
        
    # Width and height of the image, number of words per row, bits per pixel
    # in the image and palette presence
    cdef int has_palette
    
    # Output string for the sprite
    cdef char *sprite, *mask
    
    # Output string length
    cdef int sprite_length
    
    # Column and row counters
    cdef int i, j
    
    # Red, green, blue and alpha components and temporary variables
    cdef int r, g, b, a, index, bit, sprite_word
    cdef int red, green, blue
    
    # Byte offset into the image, offset into the sprite
    cdef int ptr, sprite_ptr
    
    # Pointer into the sprite output string
    cdef int k
    
    # Return objects
    cdef object obj
    
    # If there was either a palette specified or a standard one used
    # then create an inverse.
    if palette != []:

        has_palette = 1
        
        # Create inverse palette dictionary
        inverse = {}

        for i in range(0, len(palette)):

            # There may be a list of two tuples in each palette entry.
            if type(palette[i][0]) != type(0):
                inverse[palette[i][0]] = i
                inverse[palette[i][1]] = i
            else:
                # There may just be one tuple (for standard palettes).
                inverse[palette[i]] = i

        # Store the inverse palette for convenience.
        #data['inverse'] = inverse
    
    else:
    
        has_palette = 0
    
    # Write the image data to the sprite and mask
    ptr = 0

    # Allocate memory for the sprite.
    sprite_length = h_words * 4 * height
    sprite = <char *>PyMem_Malloc(sprite_length)
    
    # Sprite output pointer
    k = 0

    for j from 0 <= j < height:

        sprite_word = 0
        sprite_ptr = 0

        for i from 0 <= i < width:

            # Read the red, green and blue components
            r = <int>image[ptr] & 0xff
            g = <int>image[ptr + 1] & 0xff
            b = <int>image[ptr + 2] & 0xff

            # No alpha component
            ptr = ptr + 3

            # Write the pixels to the sprite and mask
            if bpp == 32:

                # Store the components in the sprite word
                sprite_word = r | (g << 8) | (b << 16)
                sprite_ptr = 32

            elif bpp == 16:

                # Convert the components to the relevant form
                half_word = <int>(<float>r/scale16) | \
                            (<int>(<float>g/scale16) << 5) | \
                            (<int>(<float>b/scale16) << 10)

                sprite_word = sprite_word | (half_word << sprite_ptr)
                sprite_ptr = sprite_ptr + 16

            elif bpp == 8:

                # If there is a palette then look up the colour index
                # in the inverse palette dictionary
                if has_palette == 1:

                    index = inverse[(r, g, b)]
                
                else:
                
                    # Standard palette
                    red = <int>( (<float>r) / scale8 )
                    green = <int>( (<float>g) / scale8 )
                    blue = <int>( (<float>b) / scale8 )

                    index = ((red & 0x8) << 1) | (red & 0x4) | \
                            ((green & 0x8) << 3) | ((green & 0x4) << 3) | \
                            ((blue & 0x8) << 4) | ((blue & 0x4) << 1) | \
                            <int>(<float>(red + green + blue) / 15.0)

                # Store the contents in the sprite word
                sprite_word = sprite_word | ((index & 0xff) << sprite_ptr)
                sprite_ptr = sprite_ptr + 8

            elif bpp == 4:

                # Look up bit state in inverse palette
                index = inverse[(r, g, b)]

                # Store the contents in the sprite word
                sprite_word = sprite_word | (index << sprite_ptr)
                sprite_ptr = sprite_ptr + 4

            elif bpp == 2:

                # Look up bit state in inverse palette
                index = inverse[(r, g, b)]

                # Store the contents in the sprite word
                sprite_word = sprite_word | (index << sprite_ptr)
                sprite_ptr = sprite_ptr + 2

            elif bpp == 1:

                if has_palette == 1:

                    # Look up bit state in inverse palette
                    bit = inverse[(r, g, b)]
                else:
                    # Use red component
                    if r == 255:
                        bit = 1
                    else:
                        bit = 0

                # Append bit to byte
                sprite_word = sprite_word | (bit << sprite_ptr)
                sprite_ptr = sprite_ptr + 1

            # Write the sprite word to the sprite string if the word is
            # full
            if sprite_ptr == 32:

                # End of word, so reset offset,
                sprite_ptr = 0
                # store the word in the sprite string
                sprite[k]     = (sprite_word & 0xff)
                sprite[k + 1] = ((sprite_word >> 8) & 0xff)
                sprite[k + 2] = ((sprite_word >> 16) & 0xff)
                sprite[k + 3] = ((sprite_word >> 24) & 0xff)

                k = k + 4

                # and reset the byte
                sprite_word = 0

        # Write any remaining sprite data to the sprite string
        if sprite_ptr > 0:

            # store the word in the sprite string
            sprite[k]     = <char>(sprite_word & 0xff)
            sprite[k + 1] = <char>((sprite_word >> 8) & 0xff)
            sprite[k + 2] = <char>((sprite_word >> 16) & 0xff)
            sprite[k + 3] = <char>((sprite_word >> 24) & 0xff)
            
            k = k + 4
    
    # Return sprite string
    obj = PyString_FromStringAndSize(sprite, sprite_length)
    
    PyMem_Free(sprite)
    
    return obj



cdef object _rgba2sprite(char *image, int width, int height, int h_words,
                  int bpp, object palette):

    # Width and height of the image, number of words per row, bits per pixel
    # in the image and palette presence
    cdef int has_palette
    
    # Temporary variables
    cdef int width_bits, excess_bits, excess_words
        
    # Output strings for sprite and mask
    cdef char *sprite, *mask
    
    # Output string lengths
    cdef int sprite_length, mask_length
    
    # Column and row counters
    cdef int i, j
    
    # Red, green, blue and alpha components and temporary variables
    cdef int r, g, b, a, index, bit, sprite_word, mask_word
    cdef int red, green, blue
    
    # Byte offset into the image, offsets into the sprite and mask
    cdef int ptr, sprite_ptr, mask_ptr
    
    # Pointer into the sprite and mask output strings
    cdef int k, l
    
    # Return objects
    cdef object obj
    
    # If there was either a palette specified or a standard one used
    # then create an inverse.
    if palette != []:

        has_palette = 1
        
        # Create inverse palette dictionary
        inverse = {}

        for i in range(0, len(palette)):

            # There may be a list of two tuples in each palette entry.
            if type(palette[i][0]) != type(0):
                inverse[palette[i][0]] = i
                inverse[palette[i][1]] = i
            else:
                # There may just be one tuple (for standard palettes).
                inverse[palette[i]] = i

        # Store the inverse palette for convenience.
        #data['inverse'] = inverse
    
    else:
    
        has_palette = 0
    
    # Write the image data to the sprite and mask
    ptr = 0

    # Allocate memory for the sprite.
    sprite_length = h_words * 4 * height
    sprite = <char *>PyMem_Malloc(sprite_length)
    
    # Determine the length of the mask output string in bytes.
    if bpp == 32:

        # Ensure that a whole number of words are allocated.
        mask_length = <int>(width / 32) * 4
        if width % 32 != 0:
            mask_length = mask_length + 4

    elif bpp == 16:

        # Ensure that a whole number of words are allocated.
        mask_length = <int>(width / 32) * 4
        if width % 32 != 0:
            mask_length = mask_length + 4

    elif bpp == 8:

        # Ensure that a whole number of words are allocated.
        if width % 4 == 0:

            mask_length = <int>width

        else:

            mask_length = <int>(width + 4 - (width % 4))

    elif bpp == 4:

        # Ensure that a whole number of words are allocated.
        mask_length = <int>(width / 8) * 4
        if width % 32 != 0:
            mask_length = mask_length + 4

    elif bpp == 2:

        # Ensure that a whole number of words are allocated.
        mask_length = <int>(width / 16) * 4
        if width % 32 != 0:
            mask_length = mask_length + 4

    elif bpp == 1:

        # Ensure that a whole number of words are allocated.
        mask_length = <int>(width / 32) * 4
        if width % 32 != 0:
            mask_length = mask_length + 4

    mask_length = mask_length * height

    # Allocate memory for the sprite.
    mask = <char *>PyMem_Malloc(mask_length)

    # Sprite and mask output pointers
    k = 0
    l = 0

    for j from 0 <= j < height:

        sprite_word = 0
        mask_word = 0
        sprite_ptr = 0
        mask_ptr = 0

        for i from 0 <= i < width:

            # Read the red, green and blue components
            r = <int>image[ptr] & 0xff
            g = <int>image[ptr + 1] & 0xff
            b = <int>image[ptr + 2] & 0xff

            a = <int>image[ptr + 3] & 0xff

            ptr = ptr + 4

            # Write the pixels to the sprite and mask
            if bpp == 32:

                # Store the components in the sprite word
                sprite_word = r | (g << 8) | (b << 16)
                sprite_ptr = 32

                # Store mask data.
                if a == 255:
                    a = 1
                else:
                    a = 0

                mask_word = mask_word | (a << mask_ptr)
                mask_ptr = mask_ptr + 1

            elif bpp == 16:

                # Convert the components to the relevant form
                half_word = <int>(<float>r/scale16) | \
                            (<int>(<float>g/scale16) << 5) | \
                            (<int>(<float>b/scale16) << 10)

                sprite_word = sprite_word | (half_word << sprite_ptr)
                sprite_ptr = sprite_ptr + 16

                # Store mask data.
                if a == 255:
                    a = 1
                else:
                    a = 0

                mask_word = mask_word | (a << mask_ptr)
                mask_ptr = mask_ptr + 1

            elif bpp == 8:

                # If there is a palette then look up the colour index
                # in the inverse palette dictionary
                if palette != []:

                    index = inverse[(r, g, b)]
                else:
                    # Standard palette
                    red = <int>(<float>r/scale8)
                    green = <int>(<float>g/scale8)
                    blue = <int>(<float>b/scale8)

                    index = ((red & 0x8) << 1) | (red & 0x4) | \
                            ((green & 0x8) << 3) | ((green & 0x4) << 3) | \
                            ((blue & 0x8) << 4) | ((blue & 0x4) << 1) | \
                            <int>(<float>(red + green + blue) / 15.0)

                # Store the contents in the sprite word
                sprite_word = sprite_word | (index << sprite_ptr)
                sprite_ptr = sprite_ptr + 8

                # Store mask data.
                if a != 255:
                    a = 0

                mask_word = mask_word | (a << mask_ptr)
                mask_ptr = mask_ptr + 8

            elif bpp == 4:

                # Look up bit state in inverse palette
                index = inverse[(r, g, b)]

                # Store the contents in the sprite word
                sprite_word = sprite_word | (index << sprite_ptr)
                sprite_ptr = sprite_ptr + 4

                # Store mask data.
                if a == 255:
                    a = 0xf
                else:
                    a = 0

                mask_word = mask_word | (a << mask_ptr)
                mask_ptr = mask_ptr + 4

            elif bpp == 2:

                # Look up bit state in inverse palette
                index = inverse[(r, g, b)]

                # Store the contents in the sprite word
                sprite_word = sprite_word | (index << sprite_ptr)
                sprite_ptr = sprite_ptr + 2

                # Store mask data.
                if a == 255:
                    a = 0x3
                else:
                    a = 0

                mask_word = mask_word | (a << mask_ptr)
                mask_ptr = mask_ptr + 2

            elif bpp == 1:

                if palette != []:

                    # Look up bit state in inverse palette
                    bit = inverse[(r, g, b)]
                else:
                    # Use red component
                    if bit == 255:
                        bit = 1
                    else:
                        bit = 0

                # Append bit to byte
                sprite_word = sprite_word | (bit << sprite_ptr)
                sprite_ptr = sprite_ptr + 1

                # Determine mask bit.
                if a == 255:
                    a = 1
                else:
                    a = 0

                mask_word = mask_word | (a << mask_ptr)
                mask_ptr = mask_ptr + 1

            # Write the sprite word to the sprite string if the word is
            # full
            if sprite_ptr == 32:

                # End of word, so reset offset,
                sprite_ptr = 0
                # store the word in the sprite string
                sprite[k]     = <char>(sprite_word & 0xff)
                sprite[k + 1] = <char>((sprite_word >> 8) & 0xff)
                sprite[k + 2] = <char>((sprite_word >> 16) & 0xff)
                sprite[k + 3] = <char>((sprite_word >> 24) & 0xff)

                k = k + 4

                # and reset the byte
                sprite_word = 0

            # Do the same for the mask
            if mask_ptr == 32:

                mask_ptr = 0
                mask[l]     = <char>(mask_word & 0xff)
                mask[l + 1] = <char>((mask_word >> 8) & 0xff)
                mask[l + 2] = <char>((mask_word >> 16) & 0xff)
                mask[l + 3] = <char>((mask_word >> 24) & 0xff)

                l = l + 4

                mask_word = 0

        # Write any remaining sprite data to the sprite string
        if sprite_ptr > 0:

            # store the word in the sprite string
            sprite[k]     = <char>(sprite_word & 0xff)
            sprite[k + 1] = <char>((sprite_word >> 8) & 0xff)
            sprite[k + 2] = <char>((sprite_word >> 16) & 0xff)
            sprite[k + 3] = <char>((sprite_word >> 24) & 0xff)
            
            k = k + 4

        # Do the same for the mask
        if mask_ptr > 0:

            mask[l]     = <char>(mask_word & 0xff)
            mask[l + 1] = <char>((mask_word >> 8) & 0xff)
            mask[l + 2] = <char>((mask_word >> 16) & 0xff)
            mask[l + 3] = <char>((mask_word >> 24) & 0xff)
            
            l = l + 4

    # Return sprite and mask strings
    obj = \
    [
        PyString_FromStringAndSize(sprite, sprite_length),
        PyString_FromStringAndSize(mask, mask_length)
    ]
    
    PyMem_Free(sprite)
    PyMem_Free(mask)
    
    return tuple(obj)

    
cdef object _rgb2palette(object palette):

    # Convert the palette into a string.
    
    cdef int palette_length, m
    cdef char *palette_string
    cdef object palette_output
    
    # Determine the palette length.
    palette_length = ( 2 * len(palette) * 4 )

    # Allocate memory for the palette.
    palette_string = <char *>PyMem_Malloc(palette_length)

    m = 0

    for (r1, g1, b1), (r2, g2, b2) in palette:

        palette_string[m]     = 0
        palette_string[m + 1] = r1
        palette_string[m + 2] = g1
        palette_string[m + 3] = b1
        
        palette_string[m + 4] = 0
        palette_string[m + 5] = r2
        palette_string[m + 6] = g2
        palette_string[m + 7] = b2

        m = m + 8

    palette_output = PyString_FromStringAndSize(palette_string, palette_length)
    
    PyMem_Free(palette_string)
    
    return palette_output


def rgb2sprite(data):

    # Find the sprite, mask and palette of the image.
    
    # Number of bits per pixel in the original sprite
    bpp = data['bpp']

    # If the sprite didn't have a palette then use a standard one.
    if data.has_key('palette'):

        # Explicitly defined palette
        has_palette = 1
        palette = data['palette']

    else:
        # Standard palette - invert the built in palette
        if bpp == 4:

            palette = palette16

        elif bpp == 2:

            palette = palette4

        else:
            palette = []

        # There is no explicitly defined palette
        has_palette = 0

    # Determine the actual number of words used per line of
    # the sprite.
    
    # Set the width and height variables
    width = data['width']
    height = data['height']
    
    # The number of bits in the line.
    width_bits = width * bpp
    # The excess number of bits (not filling a word completely).
    excess_bits = width_bits % 32
    # The number of excess words is therefore either one or zero.
    excess_words = excess_bits != 0
    # The number of words used.
    h_words = int( width_bits/32 ) + excess_words

    data['h_words'] = h_words
    data['v_lines'] = data['height']
    
    data['first bit'] = 0
    data['last bit'] = bpp - 1

    # Image data
    image = data['image']

    if data['mode'] == 'RGB':
    
        sprite = _rgb2sprite(image, width, height, h_words, bpp, palette)
        mask = ''
    
    elif data['mode'] == 'RGBA':
    
        sprite, mask = _rgba2sprite(image, width, height, h_words, bpp, palette)
    
    # CMYK conversion function needs to be added.
    
    if has_palette == 1:
    
        palette = _rgb2palette(palette)
    
    else:
    
        palette = ''

    return sprite, mask, palette
